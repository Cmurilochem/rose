#!/usr/bin/env python

# provides os.path.join
import os

# provides exit
import sys

# we make sure we can import runtest and runtest_config
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

# we import essential functions from the runtest library
from runtest import version_info, get_filter, cli, run

# this tells runtest how to run your code
from runtest_config import configure

# we stop the script if the major version is not compatible
assert version_info.major == 2

# construct a filter list which contains two filters
f = [
    get_filter(from_string='Partial charges of fragments',
               num_lines     = 6,
               rel_tolerance=1.0e-3),
    get_filter(from_string='Recanonicalized virtual energies of fragments',
               to_string='Reduction of Orbital Space Extent',
               abs_tolerance=1.0e-5)
    ]

# invoke the command line interface parser which returns options
options = cli()

ierr = 0
program = 'ams'
mols    = ["[water-ammonia,water-ammonia_frag0,water-ammonia_frag1]","[C2H4_C2F4_d0,C2H4_C2F4_d0_frag1,C2H4_C2F4_d0_frag2]"]
inputs  = ["[0,0,0] [1,1,1]","[0,0,0] [1,1,1]"]
for mol, input in zip(mols,inputs):
   # the run function runs the code and filters the outputs
   ierr += run(options,
               configure,
               input_files=[program,mol,input],
               filters={'stdout': f})

sys.exit(ierr)
