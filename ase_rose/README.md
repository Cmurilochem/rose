This package provides a python ASE (Atomic Simulation Environment) calculator for ROSE (Reduction of Orbital Space Extent).
This is part of the ROSE package and it is intented for ease calculation of the Intrinsic Fragment Orbitals.


# INSTALLATION:

```
$> pip install -e .
```

Or, if you don't have permission:

```
$> pip install --user -e .
```

Also, if you would like to run the ROSE-ASE tests
```
$> pip install (--user) -e ".[testing]"
```

# RUN:

Examples are provided in the tests/example directories.
