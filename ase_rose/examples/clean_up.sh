#!/bin/sh

/bin/rm *.psi4 *xyz *out* *dfcoef DFCOEF* *inp INPUT* MOLECULE.XYZ MRCONEE* *dfpcmo DFPCMO* *fchk *in fort.100 timer.dat INFO_MOL *.pyscf IAO_Fock SAO *.npy *.clean *\ 2* OUTPUT_AVAS *.chk ILMO*dat *.fcidump OUTPUT_* *.h5 *.dcoef *.dirac
