#!/bin/sh

/bin/rm *xyz *out* *dfcoef DFCOEF* *inp INPUT* MOLECULE.XYZ MRCONEE* *dfpcmo DFPCMO* *fchk *in fort.100 timer.dat INFO_MOL *.h5 IAO_Fock SAO *.npy *.clean *\ 2* OUTPUT_AVAS *.chk ILMO*dat
