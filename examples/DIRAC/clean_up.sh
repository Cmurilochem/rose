#!/bin/sh

/bin/rm *xyz *out* *dfcoef DFCOEF* *inp INPUT* MOLECULE.XYZ MRCONEE* *dfpcmo DFPCMO* *fchk fort.100 timer* INFO_MOL *.psi4 DFACMO* *dfacmo OUTPUT_AVAS* IAO_Fock* SAO* ILMO*dat *dfcoef.new *.h5*
