This program is a python interface for supported codes to generate the MO coefficient files.
This is used to test the construction of the Intrinsic Fragment Orbitals.

# INSTALLATION:

```
$> pip install -e .
```

Or, if you don't have permission:

```
$> pip install --user -e .
```

# RUN:

Examples are provided in the examples directory.
